import uvicorn
from fastapi import FastAPI
import requests

app = FastAPI()


@app.get("/")
def root():
    return {"result": "ok"}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)


new_deck = requests.get("https://deckofcardsapi.com/api/deck/new/")

print(new_deck.status_code)
print(new_deck.json)
print("test")
