# Jeu de carte - exam

# Jeu de carte 

auteur : Guillaume Pichon 

Ce projet utilise l'API de https://deckofcardsapi.com/.
L'objectif de ce script python est de proposer d'une part un webservice requetant cette API, 
et d'autre part un client de ce webservice. 

## Installation 

Pour installer les dépendances nécessaires au projet, saisir dans un terminal : 
'pip install -r requirements.txt'

## Quickstart 

Pour démarrer rapidement l'application, saisir dans un terminal : 
'uvicorn main:app --reload'
